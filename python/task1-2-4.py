### TASK 1
def funct1(string):
    #string=input("Введите строку: ")                  ### можно использовать внутри функции, но для красивого вывода вынес за пределы функции
    result_dict = dict()          ###   вместо 3-х строк можно использовать одну:
    for x in range(len(string)):   ### result_dict = {key: value for key in range(len(string)) for value in string[key]}
        result_dict[x] = string[x]  ###  это уменьшит наш код
    return result_dict
     
### TASK 2
def funct2(diction):
    keysList, valuesList = list(diction.keys()), list(diction.values())
    return f'{str(keysList)} \n{str(valuesList)}'

### TASK 4
def funct4(diction):
    inverse_dict = dict(zip(diction.values(),diction.keys()))  
    #### inverse_dict=dict([val,key] for key,val in diction.items())   ### можем использовать такой вариант
    return inverse_dict

string=input("Введите строку: ")     
mydict = funct1(string)

print(mydict, funct2(mydict), funct4(mydict), sep='\n')
