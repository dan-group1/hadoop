def max_number():
    numbers = input( "Максимальное число\nПример:\n 1,5;2,33;-450\n Введите числа без пробелов через точку с запятой: " )
    
    try:
        n = [float(x) for x in numbers.split( ';' )]
        c = max(n)
        print(int(c))
    except ValueError as k:
        print("Ошибка ввода данных.", k)

max_number()

# нужно доработать этот кусок кудо