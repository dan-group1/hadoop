def max_number_in_stroke():
    stroke = input("Максимальное число\nПример:\n 1,5;2,33;-450\n Введите числа без пробелов через точку с запятой: ")
    list = stroke.split(';')
    list_of_number = []
    for i in list:
        if '.' in i:
            try:
                list_of_number.append(float(i))
            except:
                return f'Неверное значение: {i}'
        else:
            try:
                list_of_number.append(int(i))
            except:
                return f'Неверное значение: {i}'
    return max(list_of_number)       

print(max_number_in_stroke())